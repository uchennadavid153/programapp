# app
# clone the app
```
git clone https://gitlab.com/uchennadavid153/programapp.git
```

# enter into the app folder
```
cd app-name
```
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
