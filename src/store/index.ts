import router from '@/router'
import { FirestoreService } from '@/services/firestore.service'
import { createStore } from 'vuex'

let firestoreService = new FirestoreService()

export default createStore({
  state: {
    days:<any>[],
    topics:<any>[],
    loader:false,
    user:<any>{},
    notification:<any>{},
    files:<any>[],
    extraStore:<any>[],
    live_videos:<any>[],
  },
  mutations: {
    setUser(state, payload){
      return state.user = payload
    },
 
    setDays(state, payload){
      return state.days
    },
    getDays(state, payload){
      return state.days = payload
    },
    editDays(state, payload){
      return state.days
    },
    deleteDays(state, id){
      return state.days = state.days.filter((s:any) => s.id !== id)
    },

    getLiveVideo(state, payload){
      state.live_videos = payload
    },
    

    setTopics(state, payload){
      return state.topics
    },
    getTopics(state, payload){
      return state.topics = payload
    },
    editTopic(state, payload){
      return state.topics
    },
    deleteTopic(state, id){
      return state.topics = state.topics.filter((a:any) => a.id !== id)
    },
    closeLoader(state){
      state.loader = false
    },
    showLoader(state){
      state.loader = true
    },
    getFiles(state, payload){
      return state.files = payload
    },
    setNotification(state: any, payload: any) {
      return state.notification = payload;
  },
  hideNotification(state:any){
    return state.notification = {}
  },
  setLoader(state){
    return state.loader = true
  },
  hideLoader(state){
    return state.loader = false
  },
  setExtraStore(state, payload){
    return state.extraStore = payload
  },
  getExtraStore(state){
    return state.extraStore
  },
  deleteExtraStore(state, payload){
    return state.extraStore = state.extraStore.filter((x:any) => x !== payload)
  }


  },
  actions: {
    getUser({commit}){
      let uid = localStorage.getItem('uid')
      // uid = JSON.parse(uid as string)
      firestoreService.getUser(uid).then((data) => {        
        commit('setUser', data)

      })
    },
  
    setDay({commit}, payload){
      firestoreService.createDays(payload).then(data => {
        commit('setDays', payload)
        commit('setNotification', {type:"SUCCESS", message:'Created Successfully'})
      }).catch(err => commit('setNotification', {type:"ERROR", message:'An Error Occured. No Network!'})).finally(()=> commit('hideLoader'))
    },
    editDay({commit}, {payload, day_id}){
      firestoreService.updateDay(payload, day_id).then((data)=> {
        commit('setDays', payload)
        commit('setNotification', {type:"SUCCESS", message:'Edited Successfully'})
      }).catch(err => commit('setNotification', {type:"ERROR", message:'An Error Occured. No Network!'})).finally(()=> commit('hideLoader'))
    },
    deleteDay({commit}, id){
      firestoreService.deleteDay(id).then(()=>{
        commit('setDays', id)
        commit('setNotification', {type:"SUCCESS", message:'Deleted Successfully'})
      }).catch(err => commit('setNotification', {type:"ERROR", message:'An Error Occured. No Network!'})).finally(()=> commit('hideLoader'))
    },
    getDays({commit}){     
      commit('setLoader') 
      firestoreService.getDays().then(data =>  {
        commit('getDays', data)
        // commit('hideLoader')
      }).catch(err => commit('setNotification', {type:"ERROR", message:'An Error Occured. No Network!'})).finally(()=> commit('hideLoader'))
    },

    setTopics({commit}, {payload, day_id}){      
      firestoreService.createTopics(payload, day_id).then((data) => {
        commit('setTopics')
        commit('setNotification', {type:"SUCCESS", message:'Created Successfully'})
      }).catch(err => commit('setNotification', {type:"ERROR", message:'An Error Occured. No Network!'})).finally(()=> commit('hideLoader'))
    },

    editTopics({commit}, {day_id, topic_id, payload}){
      firestoreService.editTopics(day_id, topic_id, payload).then(()=> {
        commit('setTopics')
         commit('setNotification', {type:"SUCCESS", message:'Edited Successfully'})
      }).catch(err => commit('setNotification', {type:"ERROR", message:'An Error Occured. Please try Again later!'})).finally(()=> commit('hideLoader'))
    },

    getTopics({commit}, day_id){
      commit('setLoader')
      firestoreService.getTopics(day_id).then(data => {       
        commit('getTopics', data)
      }).catch(err => commit('setNotification', {type:"ERROR", message:'An Error Occured. Please try Again later!'})).finally(()=> commit('hideLoader'))
    },
    deleteTopic({commit}, { day_id, topic_id}){
      commit('setLoader')
      firestoreService.deleteTopic(day_id, topic_id).then(()=> {
        commit('setTopics')
        commit('setNotification', {type:"SUCCESS", message:'Deleted Successfully'})
      }).catch(err => commit('setNotification', {type:"ERROR", message:'An Error Occured. Please try Again later!'})).finally(()=> commit('hideLoader'))
    },
    editVideoUrl({commit}, {day_id, topic_id, video_id, payload }){
      firestoreService.editVideoUrl(day_id, topic_id, video_id, payload).then(()=> {
        commit('setTopics')
      }).catch(err => commit('setNotification', {type:"ERROR", message:'An Error Occured. Please try Again later!'})).finally(()=> commit('hideLoader'))
    },

    setLoader({commit}){
      commit('setLoader')
    },

    hideLoader({commit}){
      commit('hideLoader')
    },

    createUser({commit}, payload){
      // console.log(payload);
      firestoreService.createUser(payload).then(data1 => {
       router.push('/dashboard')
       commit('setNotification', {type:"SUCCESS", message:'Signed In Successfully!'})
      }).catch((err)=> commit('setNotification', {type:"ERROR", message:err?.message}))
    },

    getFiles({commit}, topic_id){
      commit('setLoader')
      firestoreService.getFiles(topic_id).then(data => {
          commit('getFiles', data)
      }).catch(err => commit('setNotification', {type:"ERROR", message:'Slow Intervention to Network. Please try Again Later! '})).finally(()=> commit('hideLoader'))
    },

    setNotification({commit}, payload){
      commit('setNotification', payload)
    },
    hideNotification({commit}){
      commit('hideNotification')
    },
    addextraStore({commit}, {payload,day_id ,topic_id, files}){
      firestoreService.createVideos(payload,day_id ,topic_id, files).then((data) => {
        commit('setExtraStore', data)
      })
    },
    editextraStore({commit}, {payload,day_id ,topic_id, files}){
      commit('setLoader')
      firestoreService.createVideos(payload,day_id ,topic_id, files).then((data) => {
        commit('setExtraStore', data)
      }).catch(err => commit('setNotification', {type:"ERROR", message:'An Error Occured. Please try Again later!'})).finally(()=> commit('hideLoader'))
    
    },
    getextraStore({commit}, {day_id }){
      commit('setLoader')
      firestoreService.getTopics(day_id).then(data => {       
        commit('setExtraStore', data)
      }).catch(err => commit('setNotification', {type:"ERROR", message:'An Error Occured. Please try Again later!'})).finally(()=> commit('hideLoader'))
    
    },

    delextraStore({commit}, {day_id }){
      commit('setLoader')
      firestoreService.getTopics(day_id).then(data => {       
        commit('setExtraStore', data)
      }).catch(err => commit('setNotification', {type:"ERROR", message:'An Error Occured. Please try Again later!'})).finally(()=> commit('hideLoader'))
    },

    setLiveVideo({commit}, {payload, video_id}){
      commit('setLoader')
      firestoreService.addLiveVideos(payload, video_id).then(() =>   commit('setNotification', {type:"SUCCESS", message:'New Live Video Added Successfully'})).catch(err => commit('setNotification', {type:"ERROR", message:'Slow Intervention to Network. Please try Again Later! '})).finally(()=> commit('hideLoader'))
    },
    getLiveVideos({commit}){
      commit('setLoader')
      firestoreService.getLiveVideos().then((data)=> {
        commit('getLiveVideo', data);
      }).catch(err => commit('setNotification', {type:"ERROR", message:'Slow Intervention to Network. Please try Again Later! '})).finally(()=> commit('hideLoader'))
    },
    removeLiveVideos({commit}, id){
      commit('setLoader')
      firestoreService.removeFromLive(id).then(()=> {
        // commit('setTopics')
        commit('setNotification', {type:"SUCCESS", message:'Removed From Live Video Successfully'})
      }).catch(err => commit('setNotification', {type:"ERROR", message:'An Error Occured. Please try Again later!'})).finally(()=> commit('hideLoader'))
    }
  
  },
  getters:{
    getDays(state){return state.days},
    getTopics(state){return state.topics},
    getUser(state){return state.user},
    getNotification(state){return state.notification},
    getFiles(state){return state.files},
    getLoader(state){return state.loader},
    getExtraStore(state){return state.extraStore},
    getLiveVideos(state){return state.live_videos}
  },
  modules: {
  }
})

