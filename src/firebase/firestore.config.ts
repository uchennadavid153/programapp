// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth"
import {getFirestore} from 'firebase/firestore';
import { getStorage } from "firebase/storage";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional


// const firebaseConfig = {
//   apiKey: "AIzaSyBSYcfPyBRLAuUd_dU2fCklrj4Id4VGUAs",
//   authDomain: "vue-app-aad9d.firebaseapp.com",
//   projectId: "vue-app-aad9d",
//   storageBucket: "vue-app-aad9d.appspot.com",
//   messagingSenderId: "1077063233712",
//   appId: "1:1077063233712:web:412130e464b40506e0cb75",
//   measurementId: "G-2JCE9H6BBY"
// };

const firebaseConfig = {
  apiKey: "AIzaSyBKxSd0vFUhSLbb-DIhPvBnywH_mK2UO3c",
  authDomain: "scc-app-8801b.firebaseapp.com",
  projectId: "scc-app-8801b",
  storageBucket: "scc-app-8801b.appspot.com",
  messagingSenderId: "399248813393",
  appId: "1:399248813393:web:7d74af2f179c157851b770",
  measurementId: "G-XBYFDQXYXN"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app)
const db =  getFirestore(app)
const storage = getStorage(app, firebaseConfig.storageBucket)
export { auth, db , storage, firebaseConfig}