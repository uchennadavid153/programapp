import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import DefaultComponent from '../components/dashboard/default.vue'
import TopicsComponent from '../components/dashboard/topics.vue'
import ViewComponent from '../components/dashboard/view.vue'
import LiveVideos from '../components/dashboard/liveVideos.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect:'/login',
    
  },
  {
    path:'/login',
    name:'Login',
    component:() => import('../components/auth/auth.component.vue')
  },
  {
    path:'/dashboard',
    name:'Dashboard',
    component:() => import('../components/dashboard/main.vue'),
    children:[
      {
      path:'topics/:id',
      name:'Topics',
      component:TopicsComponent,
      children:[
        {
          path:"view/:topic_id",
          name:"View",
          component:ViewComponent,
          
        },
       
      ]
    },
      {
      path:'',
      name:'Main',
      component:DefaultComponent
    },
    {
      path:'/live',
      name:'Live',
      component:LiveVideos
    },
  
      
    ]
  },
  // {
  //   path: '/about',
  //   name: 'About',
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
