import store from '@/store';
import { createUserWithEmailAndPassword, signInWithEmailAndPassword, signOut } from 'firebase/auth'
import { addDoc, collection, deleteDoc, doc, getDocs, onSnapshot, query, setDoc, updateDoc, where } from 'firebase/firestore';
import { deleteObject, getStorage, listAll, ref, uploadBytes } from 'firebase/storage';
import { auth, db } from '../firebase/firestore.config'
import { LoginFormModel } from '../interface/login.interface'

export class FirestoreService {
 private   year:string = "2022"
    async createUser(payload: LoginFormModel) {
        try {
            let s = await createUserWithEmailAndPassword(auth, payload?.email, payload?.password)
            let data = {
                email:payload.email,
                password:payload.password,
                uid:s.user.uid,
                user:{email:s.user.email, token:s.user.displayName},
                other:s.providerId,
                createdAt:new Date(),
                updatedAt:new Date(),
                role:{
                    user:true,
                    admin:false
                }
            }
            if(payload.email.split('@')[1] === 'admin.com'){
                data.role.admin = true; 
                data.role.user = false
                await this.createUserCollection(data, data.uid)
            }else{
                await this.createUserCollection(data, data.uid)
            }
            await this.signIn(payload)
        } catch (error) {
            console.log(error);
        }
    }
    async signIn(payload: LoginFormModel) {
        try {
            await signInWithEmailAndPassword(auth, payload?.email, payload?.password)
        } catch (error) {
            console.log(error);
        }
    }

    async signOutIser() {
        try {
            await signOut(auth)
        } catch (error) {
            console.log(error);
        }
    }

    async createUserCollection(payload: Object, uid:string) {
        try {
            await setDoc(doc(db, `USERS`, `${uid}`), {_id:uid, payload})
            localStorage.setItem('uid', JSON.stringify(uid))
        } catch (error) {
            console.log(error);
        }
    }

    async getUser(uid:any){
        try {
            let userArray:any = []
            let user = query(collection(db, `USERS`), where('_id', '==', uid ))
            onSnapshot(user, (querySnapShot) => {
                querySnapShot.forEach((data)=> {
                    userArray.push(data.data())
                })
            })
            return userArray
        } catch (error) {
            console.log(error);
        }
    }


    // days
    async createDays(payload:Object){
        try {
            await addDoc(collection(db, `programs/${this.year}/days/`), payload)
        } catch (error) {
            console.log(error);
        }
    }

    async getDays(){
        try {
            let days:any[] = []
           let x = await getDocs(collection(db, `programs/${this.year}/days/`))
           x.docChanges().map(data => days.push({id:data.doc.id, data:data.doc.data()}))
            return days
        } catch (error) {
            console.log(error); 
            return error  
        }
    }
    async updateDay(payload:any, day_id:string){
        try {
          await updateDoc(doc(db, `programs/${this.year}/days/`, day_id), payload)
          return true
        } catch (error) {
          console.log(error);
        }
    }
   async deleteDay(id:string){
        try {
            await deleteDoc(doc(db, `programs/${this.year}/days/`, id))
        } catch (error) {
            console.log(error);
        }
    }

    // topics
    async createTopics(payload:any,  day_id:string){
        try {
            await setDoc(doc(db, `programs/${this.year}/days/${day_id}/topics`, `${Date.now()}`), payload)
            // await addDoc(collection(db, `programs/${this.year}/days/${day_id}/topics`), payload)
            // for (const i of payload?.data?.links) {
            //     await this.createVideos(i, day_id, t.id, payload.links)
            //   console.log('created');
            
            // }
            return true
        } catch (error) {
            console.log(error);
        }
    }

    async getTopics(day_id:string){
        try {
            // store.dispatch('setLoader')
            let topics:any[] = []
            let x = await getDocs(collection(db, `programs/${this.year}/days/${day_id}/topics`))
            x.docChanges().map(data => topics.push({id:data.doc.id, data:data.doc.data(),files: this.getVideos(day_id, data.doc.id) }))
            localStorage.setItem('store', JSON.stringify(topics))
             return topics
        } catch (error) {
            console.log(error);
            return error
        }
    }

    async getExtrastore(day_id:any, topic_id:any){
        try {
            let s:any = await this.getTopics(day_id)
            let y = s?.filter((a:any) => a.id === topic_id)
            localStorage.setItem('store', JSON.stringify(y))
            
            
            return true
        } catch (error) {
            console.log(error);
            
        }
    }
   async editTopics(day_id:any, topic_id:any, payload:any){
        try {
            let t =   await updateDoc(doc(db, `programs/${this.year}/days/${day_id}/topics`, topic_id), payload)
            return true
        } catch (error) {
            console.log(error);
        }
    }

   async deleteTopic(day_id:any, topic_id:any){
    try {
        await deleteDoc(doc(db, `programs/${this.year}/days/${day_id}/topics`, topic_id))
        return true
    } catch (error) {
        console.log(error);
    }
    }

    async createVideos(payload:{name:string, file:string[]},  day_id:string, topic_id:any, files:any[]){
        try {
            let t = await addDoc(collection(db, `programs/${this.year}/days/${day_id}/topics/${topic_id}/videos`), payload)
            for (const i of files) {
               await this.createVideosFiles(topic_id, i.name, i?.data, t.id)
            }
            
        } catch (error) {
            console.log(error);
            
        }
    }

    // extra function

     getVideos( day_id:string, topic_id:any){
        try {
            let videos:any[] = []
            let ref = `programs/${this.year}/days/${day_id}/topics/${topic_id}/videos`
            getDocs(collection(db, ref)).then(x => {
                x.docChanges().map(data => videos.push({id:data.doc.id, data:data.doc.data()}))

            })
             return videos
        } catch (error) {
            console.log(error);
        }
    }

    async editVideoUrl(day_id:string, topic_id:any, video_id:any, payload:any){
        try {
            await updateDoc(doc(db, `programs/${this.year}/days/${day_id}/topics/${topic_id}/videos`, video_id), payload)
            return true
        } catch (error) {
            console.log(error);
        }
    }

    async createVideosFiles(topic_id:any, file_name:any, file_data:any, video_id:any){
        try {
            await uploadBytes(ref(getStorage(), `DOCUMENTS/${topic_id}/file${video_id}_${file_name}`), file_data)
            console.log('created');
            
        } catch (error) {
            console.log(error);
        }
    }

    async deleteFileVideo(topic_id:any, file_name:any, video_id:any){
        try {
            await deleteObject(ref(getStorage(), `DOCUMENTS/${topic_id}/file${video_id}_${file_name}`))
        } catch (error) {
            console.log(error);
        }
    }

    async getFiles(topic_id:any){
        try {
            let ar:any[] = []
          let x =  await listAll(ref(getStorage(), `DOCUMENTS/${topic_id}/`))
          x.items.map(a => ar.push( {name:a.name, fullPath:a.fullPath, path:a.parent, storage:a.storage}))
          return ar
        } catch (error) {
            console.log(error);
        }
    }
   
    async addLiveVideos(payload:any, video_id:any){
        try {
            // await setDoc(doc(db, `live_videos`, `${video_id}`), {_id:video_id, payload});
            await updateDoc(doc(db, `live_videos`, `${video_id}`), payload)
            return true;
        } catch (error) {
            console.log(error);
        }
    }

    async getLiveVideos(){
        try {
            let live_videos:any[] = [];
            let x = await getDocs(collection(db, `live_videos`));
            x.docChanges().map(data => live_videos.push({id:data.doc.id, data:data.doc.data()}));        
             return live_videos;
        } catch (error) {
            console.log(error);
        }
    }

    async removeFromLive(id:string){
        try {
            await deleteDoc(doc(db, `live_videos`, id))
            return true
        } catch (error) {
            console.log(error);
        }
    }

    async updateLiveVideo(payload:any){
        try {
            await updateDoc(doc(db, `live_videos`, `00gdFxVybnQI5HBzc6Yp`), payload)
        } catch (error) {
            console.log(error);
            
        }
    }
}
